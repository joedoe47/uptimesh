#!/bin/bash

#This is an uptime script that will send a bunch of emails when a service goes down
#The idea is a crontab runs every so often executing this script (or a gitlab scheduled task) and checks these sites. If a site is down it creates an issue on gitlab/github, then sending an email to a personal account(s).
#to change the schedule of how often this script is run, please run " crontab -e " to adjust how often we check that these sites are up.

#sites that we check. (new sites must have a new line as such)
SITES="
https://www.miamisprings-fl.gov/
https://mail.miamisprings-fl.gov/owa/index.html
https://miamispringspolice.com/owa/index.html
"

#if a site is down touch a lock file to annoy less often but make tickets in gitlab and zendesk
createissue(){
test -f /tmp/uptimesh/"${1}".lock || { touch /tmp/"${1}".lock ; curl --ssl-reqd --url 'smtps://smtp.gmail.com:465' --user 'mendizaj1111@gmail.com:PASSWORD' --mail-from 'mendizaj1111@gmail.com' --mail-rcpt 'YOURTICKETSYSTEMHERE@incoming.gitlab.com' --mail-rcpt 'support@zendesk.com' -H "Subject: $1 is down!" -F text="AWS has failed to reach $1. It might be down." ; }
}

#test and if not create the folder we will work on (if the folder exists it won't bother)
test -d /tmp/uptimesh/ || { mkdir -p /tmp/uptimesh/ ;}

#this is all it takes. KISS principal.
for X in $SITES; do echo "checking $X"; curl -o /dev/null "$X" || createissue "$X"; done
