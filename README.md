# uptimesh

A simple bash script operated via cron to keep an eye on hosts.

## How to install

    $ git clone https://gitlab.com/joedoe47/uptimesh.git
    $ cd uptimesh

- You need to edit "crontab" and change "/home/YOURUSERNAMEHERE/" to the path where you cloned this script. 
- You also need to edit "daily.sh" 
    - and add or remove hosts of interest in the variable labeled "SITES" just type the link 
    - also change the --mail-rcpt (the people who will get your alert) and --url (the stmp server) and --user (the login to the stmp server) and --mail-from

    $ crontab < crontab.txt

and thats it. I know its a lot but this is set it and forget it software. I love that kind of software the most. 

### Side Note:

This script monitors sites every 4 hours and then sends 1 alert every 24 hours. This is done to minimize alert spam and excessive ticket creation on my system anyways. Please adjust it to your needs. 
